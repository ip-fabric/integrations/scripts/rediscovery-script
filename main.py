import argparse
import csv
from slugify import slugify
from ipfabric import IPFClient
import time
import os
from dotenv import load_dotenv


def csv_to_json(file):
    devices = []
    csvReader = csv.DictReader(file)
    new_row = {}
    serials = []
    for row in csvReader:
        for k, v in row.items():
            new_row[slugify(k)] = v
        serials.append(new_row['serial-number'])
        devices.append(new_row)
    return devices, serials

def get_last_snapshot(ipf):
    snapshot_id = next(iter(ipf.loaded_snapshots))
    return snapshot_id

def clone_and_load(ipf):
    snapshot_id = get_last_snapshot(ipf)
    resp = ipf.post(f'snapshots/{snapshot_id}/clone')
    if resp.status_code == 204:
        cloning = True
        new_snapshot_id = ""
        new_snapshot = None
        is_loaded = True
        while cloning:
            ipf.unloaded = True
            all_snapshots = ipf.get_snapshots()
            for k, v in all_snapshots.items():
                if 'clone' in v.name and v.start == all_snapshots[snapshot_id].start:
                    print(f"Cloned snapshot found: {v.snapshot_id} parent: {snapshot_id}")
                    new_snapshot_id = k
                    new_snapshot = v
                    cloning = False

        if not cloning:
            print("Finding cloning jobs and checking if it is complete")
            filter = dict(snapshot=["eq", snapshot_id], name=["eq", "snapshotClone"])
            job = ipf.jobs._return_job_when_done(job_filter=filter, retry=100, timeout=30)
            if job:
                print(job)
                    

        print("Completed Cloning Load Beginning")

        load = new_snapshot.load(ipf, timeout=60, retry=100, wait_for_assurance=False)
        while is_loaded:
            time.sleep(2)
            print("Checking if snapshot is loaded")
            if ipf.inventory.devices.count(snapshot_id=new_snapshot_id) > 0:
                is_loaded = False

        if is_loaded == False and new_snapshot_id:
            return new_snapshot_id

def main():
    load_dotenv()
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", type=argparse.FileType('r'))
    args = parser.parse_args()
    devices, serials = csv_to_json(args.file)
    ipf = IPFClient()
    cloned_snapshot_id = clone_and_load(ipf)
    if cloned_snapshot_id:
        print(f"Rediscover {len(serials)} devices.")
        data = {"snList": serials}
        resp = ipf.post(f"snapshots/{cloned_snapshot_id}/devices", json=data)


if __name__ == "__main__":
    main()