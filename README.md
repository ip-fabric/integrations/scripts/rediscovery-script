# IP Fabric Rediscovery Script

**Integrations or scripts should not be installed directly on the IP Fabric VM unless directly communicated from the
IP Fabric Support or Solution Architect teams.  Any action on the Command-Line Interface (CLI) using the root, osadmin,
or autoboss account may cause irreversible, detrimental changes to the product and can render the system unusable.**

## Configuration

```commandline
cp .env-example .env
```

Edit .env with your IPF instance's URL, token, and set verify=false if required.

## Installation

```commandline
pip install -r requirements.txt
```

## Device CSV
Navigate to `Inventory -> Devices` apply a filter to only show the devices to rediscover and export the csv.

## Running

```commandline
python main.py --file devices.csv
```
